module.exports= (sequelize, Sequelize)=>{
    const Note = sequelize.define('notes',{
        uuid:{
            type: Sequelize.UUID,
            defaultValue: Sequelize.UUIDV4,
            primaryKey:true
        },
        text:{
            type: Sequelize.STRING,
            required:true,
            allowNull:false,
            validate:{
                notNull:{
                    msg: 'content is null'
                },
                notEmpty:{
                    msg:'content is an empty string'
                }
            }
        },
        color:{
            type: Sequelize.STRING,
            required:true,
            defaultValue:'#ffffff',
            allowNull:false,
            validate:{
                notNull:{
                    msg:'A color must be set'
                }
            }
        },
        favorite:{
            type: Sequelize.BOOLEAN,
            defaultValue: false
        }
    });
    return Note;
}