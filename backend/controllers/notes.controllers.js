const db = require('../db.config');
const Note = db.Note;
const utils = require('../utils/notes.utils');

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.notes_post=function(req,res){
   Note.create(req.body).then(
        note=>{
            res.status(201).json({text:note.text, color: note.color, favorite: note.favorite, createdAt: note.createdAt});
        }
    ).catch(error=>{
        res.status(400).json({'error': utils.handleErrors(error)});
    });
}
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.notes_get=function(req, res){
    const attributes= ['uuid','text','color','favorite','createdAt']
    Note.findAll(req.query.id ? {where: {uuid: req.query.id},attributes: attributes} : {attributes: attributes}).then(notes=>{
        res.status(200).json(notes);
    }).catch(error=>{
        res.status(400).json({error:utils.handleErrors(error)});
    });
}

/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.update_note= function(req, res){
    Note.findByPk(req.query.id ).then(note=>{
        let body = req.body;
        note.text= body.text || note.text;
        note.color=body.color || note.color;
        note.favorite=body.favorite || note.favorite;
        note.save();
        res.status(200).json({text:note.text, color: note.color, favorite: note.favorite, createdAt: note.createdAt});
    }).catch(error=>{
        res.status(400).json({error:utils.handleErrors(error)});
    });
}
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */
exports.delete_note=function(req, res){
    Note.findByPk(req.query.id).then(note=>{
        note.destroy();
        res.status(200).json({'msg':'deleted!'});
    }).catch(error=>{
        res.status(400).json({'error':utils.handleErrors(error)});
    });
}