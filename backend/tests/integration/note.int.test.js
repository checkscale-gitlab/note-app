const app = require('../../app');
const supertest = require('supertest');
const sequelize = require('sequelize');
const request = supertest(app);
const noteMock=require('../mockData/note-mock.json');
const resolvedNote = require('../mockData/note-resolved.json');
const db=require('../../db.config');
const Note = db.Note;
beforeEach(async ()=>{
    await db.sequelize.sync({force:true});
});
describe('POST /notes test suite',()=>{
    test('Must return 201 when correct',async ()=>{
       let response = await request.post('/notes').send(noteMock);
       expect(response.statusCode).toBe(201);
    });
    test('Right json body response must be defined',async()=>{
        const response = await request.post('/notes').send(noteMock);
        expect(response.body).toBeTruthy();
    });
    test('Must return 400 status code when fields are missing', async ()=>{
        let response = await request.post('/notes').send({});
        expect(response.statusCode).toBe(400);
    });
});

describe('GET /notes test suite', ()=>{
    test('Must return 200 when correct',async()=>{
        let response = await request.get('/notes');
        expect(response.statusCode).toBe(200);
    });
    test('Must return 400 error code when something was wrong', async()=>{
        let response = await request.get('/notes?id=asda');
        expect(response.statusCode).toBe(400);
    })
    test('Must return one register when id query string is passed', async()=>{
        let note = await Note.create(noteMock);
        const response = await request.get(`/notes?id=${note.uuid}`);
        expect(response.body).toBeTruthy();
    });
    test('Must return all register when query string is not passed', async()=>{
        await Note.create(noteMock);
        await Note.create(noteMock);
        let response =await request.get('/notes');
        expect(response.body).toBeTruthy();
    })
});

describe('PATCH /notes test suite',()=>{
    test('Must return 200 status code when correct', async ()=>{
        let note = await Note.create(noteMock);
        let response  = await request.patch(`/notes?id=${note.uuid}`);
        expect(response.statusCode).toBe(200);
    });
    test('Must return 400 status code when wrong', async ()=>{
        let response = await request.patch('/notes?id=');
        expect(response.statusCode).toBe(400);
    });
    test('Update field ', async ()=>{
        let note = await Note.create(noteMock);
        let fixedText = 'This works right!';
        let response = await request.patch(`/notes?id=${note.uuid}`).send({text: fixedText});
        expect(response.body.text).toEqual(fixedText);
    });
});