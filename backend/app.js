const cors = require('cors');
const express = require('express');
const router = require('./routes/notes.routes');
const db = require('./db.config');
const app = express();
const corsOptions = {"Accept":"application/json"}
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(cors(corsOptions));
app.use(router);
module.exports = app;